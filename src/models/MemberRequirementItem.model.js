const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'member_requirement_items'

module.exports.MemberRequirementItem = db.define(table, {
    id: {
        type: Sequelize.NUMBER,
        primaryKey: true
    },
    member_id: {
        type: Sequelize.NUMBER
    },
    requirement_id: {
        type: Sequelize.NUMBER
    },
    club_id: {
        type: Sequelize.NUMBER
    },
    answer: {
        type: Sequelize.TEXT
    }
},
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log('afterCreate MemberRequirementItem')
            },
            afterUpdate: function (item, fn) {
                console.log('afterUpdate MemberRequirementItem')
            }
        }
    }
)