const Sequelize = require('sequelize')
const db = require('../database/database')
const TitleRegularClassEvents = require('../events/TitleRegularClass.events')

const table = 'title_regular_classes'

module.exports.TitleRegularClass = db.define(table, {
    id: {
        type: Sequelize.NUMBER,
        primaryKey: true
    },
    title: {
        type: Sequelize.TEXT
    }
},
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                TitleRegularClassEvents.afterCreate()
            },
            afterUpdate: function (item, fn) {
                TitleRegularClassEvents.afterUpdate()
            }
        }
    }
)