const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'member_requirements'

module.exports.MemberRequirement = db.define(table, {
    id: {
        type: Sequelize.NUMBER,
        primaryKey: true
    },
    member_id: {
        type: Sequelize.NUMBER
    },
    requirement_id: {
        type: Sequelize.NUMBER
    },
    club_id: {
        type: Sequelize.NUMBER
    },
    answer: {
        type: Sequelize.TEXT
    }
},
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log('afterCreate MemberRequirement')
            },
            afterUpdate: function (item, fn) {
                console.log('afterUpdate MemberRequirement')
            }
        }
    }
)