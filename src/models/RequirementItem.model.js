const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'requeriment_items'

module.exports.RequirementItem = db.define(table, {
    id: {
        type: Sequelize.NUMBER,
        primaryKey: true
    },
    requiremento_id: {
        type: Sequelize.NUMBER
    },
    description: {
        type: Sequelize.TEXT
    },
    descriptive: {
        type: Sequelize.BOOLEAN
    },
    status: {
        type: Sequelize.BOOLEAN
    },
    video_url: {
        type: Sequelize.TEXT
    },

},
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log('afterCreate RequirementItem')
            },
            afterUpdate: function (item, fn) {
                console.log('afterUpdate RequirementItem')
            }
        }
    }
)