const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const http = require('http');

const app = express();
const server = http.Server(app);

//config
app.use(cors());
app.use(express.json());
app.use(routes);

server.listen(3333);