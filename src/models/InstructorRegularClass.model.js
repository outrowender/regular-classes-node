const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'instructor_regular_classes'

module.exports.InstructorRegularClass = db.define(table,
    {
        id: {
            primaryKey: true,
            type: Sequelize.NUMBER
        },
        regular_class_id: {
            type: Sequelize.NUMBER,
        },
        member_id: {
            type: Sequelize.NUMBER,
        },
        club_id: {
            type: Sequelize.NUMBER,
        },
        video_url: {
            type: Sequelize.STRING,
        },

    },
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log('afterCreate instructorRegularClass')
            },
            afterUpdate: function (item, fn) {
                console.log('afterUpdate instructorRegularClass')
            }
        }
    }
)