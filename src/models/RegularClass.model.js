const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'regular_classes'

module.exports.RegularClass = db.define(table, {
    id: {
        type: Sequelize.NUMBER,
        primaryKey: true
    },
    description: {
        type: Sequelize.TEXT
    },
    age: {
        type: Sequelize.NUMBER
    },
    order: {
        type: Sequelize.NUMBER
    }
},
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log('afterCreate RegularClass')
            },
            afterUpdate: function (item, fn) {
                console.log('afterUpdate RegularClass')
            }
        }
    }
)