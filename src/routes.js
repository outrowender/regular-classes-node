const express = require('express');
const TitleRegularClassesController = require('./controllers/TitleRegularClass.controller');

const routes = express.Router();

routes.get('/titleregularclass', TitleRegularClassesController.index);
routes.get('/titleregularclass/:id', TitleRegularClassesController.show);
routes.post('/titleregularclass', TitleRegularClassesController.store);

module.exports = routes;