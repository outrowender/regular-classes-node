const { TitleRegularClass } = require('../models/TitleRegularClass.model')

module.exports = {
    async index(req, res) {
        const result = await TitleRegularClass.findAll()
        return res.status(200).json(result);
    },
    async show(req, res) {
        const { id } = req.params
        const result = await TitleRegularClass.findByPk(id)
        return res.status(200).json(result);
    },
    async store(req, res) {
        const { id, title } = req.body
        const result = await TitleRegularClass.create({ id, title })
        return res.status(200).json(result);
    },
    async update(req, res) {
        //não implmentado
        return res.status(200).json(result);
    }

}