const Sequelize = require('sequelize')
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/database.json')[env];

const db = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect
})

db.authenticate().then(function () {
    console.log(`conected to ${config.database}`)
}).catch(function (e) {
    console.log(`Error: ${e.message}`)
})

module.exports = db