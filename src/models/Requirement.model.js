const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'requirements'

module.exports.Requirement = db.define(table, {
    id: {
        type: Sequelize.NUMBER,
        primaryKey: true
    },
    regular_class_id: {
        type: Sequelize.NUMBER
    },
    title_regular_class_id: {
        type: Sequelize.NUMBER
    },
    advanced: {
        type: Sequelize.BOOLEAN
    },
    description: {
        type: Sequelize.TEXT
    },
    descriptive: {
        type: Sequelize.BOOLEAN
    },
    status: {
        type: Sequelize.BOOLEAN
    }
},
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log(titleRegularClass)
                console.log('afterCreate')
            },
            afterUpdate: function (item, fn) {
                console.log(titleRegularClass)
                console.log('afterUpdate')
            }
        }
    }
)