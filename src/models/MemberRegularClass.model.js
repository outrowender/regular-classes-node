const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'member_regular_classes'

module.exports.MemberRegularClass = db.define(table,
    {
        id: {
            type: Sequelize.NUMBER,
            primaryKey: true
        },
        regular_class_id: {
            type: Sequelize.NUMBER
        },
        club_id: {
            type: Sequelize.NUMBER
        },
        member_id: {
            type: Sequelize.NUMBER
        },
        date_start: {
            type: Sequelize.DATE
        },
        completed: {
            type: Sequelize.BOOLEAN
        }
    },
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log('afterCreate MemberRegularClass')
            },
            afterUpdate: function (item, fn) {
                console.log('afterUpdate MemberRegularClass')
            }
        }
    }
)