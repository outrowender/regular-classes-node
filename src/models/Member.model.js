const Sequelize = require('sequelize')
const db = require('../database/database')

const table = 'members'

module.exports.Member = db.define(table,
    {
        id: {
            type: Sequelize.NUMBER,
            primaryKey: true
        }
    },
    {
        timestamps: false,
        hooks: {
            afterCreate: function (item, fn) {
                console.log('afterCreate member')
            },
            afterUpdate: function (item, fn) {
                console.log('afterUpdate member')
            }
        }
    }
)